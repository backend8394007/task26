const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Form = require('./models/form');

const app = express();
const db = 'mongodb+srv://Eugene:23092004@cluster0.mgxwzah.mongodb.net/task26?retryWrites=true&w=majority';

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

mongoose
    .connect(db)
    .then(() => console.log('Connected to DB'))
    .catch((err) => console.log(err));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('/form/create', (req, res) =>{
   res.render('create', {button: 'Отправить', message: ''});
});

app.get('/form/update', (req, res) =>{
    res.render('update', {button: 'Обновить', message: ''});
});

app.get('/form/delete', (req, res) =>{
    res.render('get_delete', {button: 'Удалить', title: 'Удалить данные', method: 'delete', message: ''});
});

app.get('/form/get', (req, res) =>{
    res.render('get_delete', {button: 'Получить', title: 'Получить данные', method: 'get', message: ''});
});

app.post('/form/create', async (req, res) =>{
    const {name, age, password} = req.body;
    try {
        const hashedPassword = await bcrypt.hash(password, 10);
        const form = new Form({name, age, password: hashedPassword});
        const user = await Form.findOne({name});
        if (!user){
            await form.save();
            res.status(201).render('create', {button: 'Отправить', message: 'Данные сохранены'});
        }
        else {
            res.render('create', {button: 'Отправить', message: 'Пользователь уже существует'});
        }
    }
    catch (error) {
        res.status(500).render('create', {button: 'Отправить', message: 'Ошибка сохранения данных'});
        console.error('Internal Server Error');
    }
});

app.post('/form/get', async (req, res) => {
    const {name, password} = req.body;
    try {
        const user = await Form.findOne({name});
        if (user) {
            const passwordMatch = await bcrypt.compare(password, user.password);
            if (passwordMatch) {
                res.render('user_info', {user, password});
            } else {
                res.render('get_delete', {button: 'Получить', title: 'Получить данные', method: 'get', message: 'Неверный пароль'});
            }
        } else {
            res.render('get_delete', {button: 'Получить', title: 'Получить данные', method: 'get', message: 'Пользователь не найден'});
        }
    } catch (err) {
        res.status(500).render('get_delete', {button: 'Получить', title: 'Получить данные', method: 'get', message: 'Ошибка получения данных'});
        console.error('Internal Server Error');
    }
});

app.post('/form/delete', async (req, res) => {
    const {name, password} = req.body;
    try {
        const user = await Form.findOne({ name });
        if (user) {
            const passwordMatch = await bcrypt.compare(password, user.password);
            if (passwordMatch) {
                await Form.deleteOne({ name });
                res.render('get_delete', {button: 'Удалить', title: 'Удалить данные', method: 'delete', message: 'Данные пользователя удалены'});
            } else {
                res.render('get_delete', {button: 'Удалить', title: 'Удалить данные', method: 'delete', message: 'Неверный пароль'});
            }
        } else {
            res.render('get_delete', {button: 'Удалить', title: 'Удалить данные', method: 'delete', message: 'Пользователь не найден'});
        }
    } catch (err) {
        res.status(500).render('get_delete', {button: 'Удалить', title: 'Удалить данные', method: 'delete', message: 'Ошибка удаления данных'});
        console.error('Internal Server Error');
    }
});

app.post('/form/update', async (req, res) => {
    const {name, age, password, newPassword} = req.body;
    try {
        const user = await Form.findOne({name});
        if (user) {
            const passwordMatch = await bcrypt.compare(password, user.password);
            if (passwordMatch) {
                const hashedNewPassword = await bcrypt.hash(newPassword, 10);
                await Form.findOneAndUpdate({name}, {age, password: hashedNewPassword});
                res.render('update', {button: 'Обновить', message: 'Данные пользователя обновлены'});
            } else {
                res.render('update', {button: 'Обновить', message: 'Неверный пароль'});
            }
        } else {
            res.render('update', {button: 'Обновить', message: 'Пользователь не найден'});
        }
    } catch (err) {
        res.status(500).render('update', {button: 'Обновить', message: 'Ошибка обновления данных'});
        console.error('Internal Server Error');
    }
});

const port = process.env.PORT || 3000;

app.listen(port, (err) => {
    err ? console.log(err) : console.log(`The server is listening on port: ${port}`)
});
