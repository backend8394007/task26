const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const formSchema = new Schema({
    name:
    {
        type: String,
        required: true
    },
    age:
    {
        type: Number,
        required: false
    },
    password:
    {
        type: String,
        required: true
    },
    newPassword:
    {
            type: String,
            required: false
    }
});

const Form = mongoose.model('Form', formSchema);

module.exports = Form;